resource "aws_dynamodb_table" "terraform-lock" {
  name           = var.dynamodb_backend_table_name
  read_capacity  = 2
  write_capacity = 2
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = var.default_tags
}