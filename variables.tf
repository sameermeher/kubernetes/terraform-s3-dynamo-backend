variable "s3_backend_bucket_name" {
  type    = string
  default = "eksdemo-terraform-state-8283"
}

variable "s3_backend_bucket_key" {
  type    = string
  default = "backend-s3-dynamodb/terraform.tfstate"
}

variable "dynamodb_backend_table_name" {
  type    = string
  default = "eksdemo-terraform-state-8283"
}

variable "region" {
  type    = string
  default = "us-east-1"
}

variable "default_tags" {
  type = map(any)
  default = {
    "environment-type" = "PRE-PRODUCTION",
    "resource-owner"   = "Sameer Meher"
  }
}