terraform {

  required_version = "~> 0.14"

  backend "s3" {
    bucket                  = "eksdemo-terraform-state-8283"
    key                     = "backend-s3-dynamodb/terraform.tfstate"
    region                  = "us-east-1"
    dynamodb_table          = "eksdemo-terraform-state-8283"
    encrypt                 = true
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "default"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.33.0"
    }
  }
}