resource "aws_s3_bucket" "bucket" {
  bucket = var.s3_backend_bucket_name
  versioning {
    enabled = false
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  tags = var.default_tags
}